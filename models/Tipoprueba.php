<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipoprueba".
 *
 * @property int $codTip
 * @property string $descTip
 *
 * @property Prueba[] $pruebas
 */
class Tipoprueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipoprueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descTip'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codTip' => 'Cod Tip',
            'descTip' => 'Desc Tip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codTip' => 'codTip']);
    }
}
