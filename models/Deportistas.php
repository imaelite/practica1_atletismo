<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deportistas".
 *
 * @property int $codDep
 * @property string $nomApDep
 * @property string $provinciaDep
 * @property string $fechaNacimientoDep
 * @property string $dniDep
 * @property string $domicilioDep
 * @property int $codPosDep
 * @property string $telefonoDep
 *
 * @property Resultado[] $resultados
 */
class Deportistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deportistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimientoDep'], 'safe'],
            [['codPosDep'], 'integer'],
            [['nomApDep'], 'string', 'max' => 100],
            [['provinciaDep', 'dniDep', 'domicilioDep'], 'string', 'max' => 50],
            [['telefonoDep'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codDep' => 'Cod Dep',
            'nomApDep' => 'Nom Ap Dep',
            'provinciaDep' => 'Provincia Dep',
            'fechaNacimientoDep' => 'Fecha Nacimiento Dep',
            'dniDep' => 'Dni Dep',
            'domicilioDep' => 'Domicilio Dep',
            'codPosDep' => 'Cod Pos Dep',
            'telefonoDep' => 'Telefono Dep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['codDep' => 'codDep']);
    }
}
