<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resultado".
 *
 * @property int $codResultado
 * @property int $codPrueba
 * @property int $inscripcion
 * @property int $codDep
 * @property string $marcadep
 * @property string $posDep
 *
 * @property Deportistas $codDep0
 */
class Resultado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resultado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codPrueba', 'inscripcion', 'codDep'], 'integer'],
            [['marcadep', 'posDep'], 'string', 'max' => 50],
            [['codPrueba', 'inscripcion'], 'unique', 'targetAttribute' => ['codPrueba', 'inscripcion']],
            [['codDep'], 'exist', 'skipOnError' => true, 'targetClass' => Deportistas::className(), 'targetAttribute' => ['codDep' => 'codDep']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codResultado' => 'Cod Resultado',
            'codPrueba' => 'Cod Prueba',
            'inscripcion' => 'Inscripcion',
            'codDep' => 'Cod Dep',
            'marcadep' => 'Marcadep',
            'posDep' => 'Pos Dep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodDep0()
    {
        return $this->hasOne(Deportistas::className(), ['codDep' => 'codDep']);
    }
}
