<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prueba".
 *
 * @property int $codPrueba
 * @property int $codReu
 * @property int $numPrueba
 * @property int $codTip
 * @property string $horaPru
 * @property string $lugarPru
 *
 * @property Reunion $codReu0
 * @property Tipoprueba $codTip0
 */
class Prueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codReu', 'numPrueba', 'codTip'], 'integer'],
            [['horaPru'], 'safe'],
            [['lugarPru'], 'string', 'max' => 50],
            [['codReu', 'numPrueba'], 'unique', 'targetAttribute' => ['codReu', 'numPrueba']],
            [['codReu'], 'exist', 'skipOnError' => true, 'targetClass' => Reunion::className(), 'targetAttribute' => ['codReu' => 'codReu']],
            [['codTip'], 'exist', 'skipOnError' => true, 'targetClass' => Tipoprueba::className(), 'targetAttribute' => ['codTip' => 'codTip']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codPrueba' => 'Cod Prueba',
            'codReu' => 'Cod Reu',
            'numPrueba' => 'Num Prueba',
            'codTip' => 'Cod Tip',
            'horaPru' => 'Hora Pru',
            'lugarPru' => 'Lugar Pru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodReu0()
    {
        return $this->hasOne(Reunion::className(), ['codReu' => 'codReu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodTip0()
    {
        return $this->hasOne(Tipoprueba::className(), ['codTip' => 'codTip']);
    }
}
