<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reunion".
 *
 * @property int $codReu
 * @property string $fechaReu
 * @property string $lugarReu
 * @property string $nombreReu
 *
 * @property Prueba[] $pruebas
 */
class Reunion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reunion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaReu'], 'safe'],
            [['lugarReu', 'nombreReu'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codReu' => 'Cod Reu',
            'fechaReu' => 'Fecha Reu',
            'lugarReu' => 'Lugar Reu',
            'nombreReu' => 'Nombre Reu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codReu' => 'codReu']);
    }
}
