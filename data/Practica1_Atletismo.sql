﻿DROP DATABASE IF EXISTS practica1_atletismo;
CREATE DATABASE practica1_atletismo;
USE practica1_atletismo;

CREATE TABLE reunion(
  codReu int AUTO_INCREMENT,
  fechaReu date,
  lugarReu varchar(50),
  nombreReu varchar(50),
  PRIMARY KEY(codReu)  
  );

CREATE TABLE tipoPrueba(
  codTip int AUTO_INCREMENT,
  descTip varchar(50),
  PRIMARY KEY(codTip) 
  );

CREATE TABLE prueba(
  codPrueba int AUTO_INCREMENT,
  codReu int,
  numPrueba int,
  codTip int,
  horaPru time,
  lugarPru varchar(50),
  PRIMARY KEY(codPrueba)
  );

CREATE TABLE resultado(
  codResultado int AUTO_INCREMENT,
  codPrueba int,
  inscripcion int,
  codDep int,
  marcadep varchar(50),
  posDep varchar(50),
  PRIMARY KEY(codResultado)
  );

CREATE TABLE deportistas(
  codDep int AUTO_INCREMENT,
  nomApDep varchar(100),
  provinciaDep varchar(50),
  fechaNacimientoDep date,
  dniDep varchar(50),
  domicilioDep varchar(50),
  codPosDep int,
  telefonoDep varchar(12),
  PRIMARY KEY (codDep)
  );

-- Prueba
ALTER TABLE prueba ADD CONSTRAINT FKPruebaTipoPrueba FOREIGN KEY (codTip) REFERENCES tipoPrueba(codTip) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE prueba ADD CONSTRAINT FKPruebaReunion FOREIGN KEY (codReu) REFERENCES reunion(codReu) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE prueba ADD CONSTRAINT UniquePruebaTipo UNIQUE (codReu,numPrueba); 

-- Resultado
ALTER TABLE resultado ADD CONSTRAINT FKResultadoDeportista FOREIGN KEY (codDep) REFERENCES deportistas(codDep) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE resultado ADD CONSTRAINT UniqueResultadoPrueba UNIQUE (codPrueba,inscripcion);

--



