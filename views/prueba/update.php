<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prueba */

$this->title = 'Actualizar Prueba: ' . $model->codPrueba;
$this->params['breadcrumbs'][] = ['label' => 'Pruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codPrueba, 'url' => ['view', 'id' => $model->codPrueba]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prueba-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
