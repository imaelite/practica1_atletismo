<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TipoPrueba;
use app\models\Reunion;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Prueba */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prueba-form">

    <?php $form = ActiveForm::begin(); ?>


    
        
        <?php
    $habi = Reunion::find()->all();
    $listado = ArrayHelper::map($habi,'codReu','nombreReu');

    echo $form->field($model,'codReu')->dropDownList(
            $listado, ['prompt' => 'Selecciona la reunion']
    );
 
    
    ?>
    

    <?= $form->field($model, 'numPrueba')->textInput() ?>

    
    
        <?php
    $habi = TipoPrueba::find()->all();
    $listado = ArrayHelper::map($habi,'codTip','descTip');

    echo $form->field($model,'codTip')->dropDownList(
            $listado, ['prompt' => 'Selecciona el tipo de prueba']
    );
 
    
    ?>

    <?= $form->field($model, 'horaPru')->textInput() ?>

    <?= $form->field($model, 'lugarPru')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
