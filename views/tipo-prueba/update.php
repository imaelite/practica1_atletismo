<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoPrueba */

$this->title = 'Actualizar Tipo Prueba: ' . $model->codTip;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Pruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codTip, 'url' => ['view', 'id' => $model->codTip]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipo-prueba-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
