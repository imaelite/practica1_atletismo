<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TipoPrueba */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-prueba-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descTip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
