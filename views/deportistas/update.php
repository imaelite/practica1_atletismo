<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Deportistas */

$this->title = 'Actualizar Deportistas: ' . $model->codDep;
$this->params['breadcrumbs'][] = ['label' => 'Deportistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codDep, 'url' => ['view', 'id' => $model->codDep]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deportistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
