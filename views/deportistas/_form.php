<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Deportistas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deportistas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomApDep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provinciaDep')->textInput(['maxlength' => true]) ?>

    
    
    <?= $form->field($model, 'fechaNacimientoDep')->widget(DatePicker::className(), [
        'options' => ['class' => 'form-control'],
        'language'=>'es',
        'dateFormat'=>'yyyy-MM-dd',
        'clientOptions'=>['showAnim'=>'size','showButtonPanel'=>true]
]) ?>

    <?= $form->field($model, 'dniDep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domicilioDep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codPosDep')->textInput() ?>

    <?= $form->field($model, 'telefonoDep')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
