<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deportistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deportistas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Deportistas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codDep',
            'nomApDep',
            'provinciaDep',
            'fechaNacimientoDep',
            'dniDep',
            //'domicilioDep',
            //'codPosDep',
            //'telefonoDep',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
