<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Deportistas */

$this->title = $model->codDep;
$this->params['breadcrumbs'][] = ['label' => 'Deportistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deportistas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codDep], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->codDep], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro de querer borrar el elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codDep',
            'nomApDep',
            'provinciaDep',
            'fechaNacimientoDep',
            'dniDep',
            'domicilioDep',
            'codPosDep',
            'telefonoDep',
        ],
    ]) ?>

</div>
