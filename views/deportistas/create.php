<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Deportistas */

$this->title = 'Crear Deportistas';
$this->params['breadcrumbs'][] = ['label' => 'Deportistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deportistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
