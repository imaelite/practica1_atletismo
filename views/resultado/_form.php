<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Prueba;
use app\models\Deportistas;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Resultado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resultado-form">

    <?php $form = ActiveForm::begin(); ?>

    
            
        <?php
    $habi = Prueba::find()->all();
    $listado = ArrayHelper::map($habi,'codPrueba','numPrueba');

    echo $form->field($model,'codPrueba')->dropDownList(
            $listado, ['prompt' => 'Selecciona la prueba']
    );
 
    
    ?>
    
    

    <?= $form->field($model, 'inscripcion')->textInput() ?>


    
        
    <?php
    $habi = Deportistas::find()->all();
    $listado = ArrayHelper::map($habi,'codDep','nomApDep');

    echo $form->field($model,'codDep')->dropDownList(
            $listado, ['prompt' => 'Selecciona el jugador']
    );
 
    
    ?>


    <?= $form->field($model, 'marcadep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posDep')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
